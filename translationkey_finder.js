//Put this file in the root of a portal and start
// $ node translationkey_finder.js

//Default file is en_GB.json, if you don't have it in your project, this script will fail
var fs = require('fs');
var path = require('path');
var fileList, unusedKeys;

//TODO: Load this from the translations/en_GB.json
var translationKeyList = [];

//LOAD ALL FILE LIST RECURSIVELY
//https://gist.github.com/kethinov/6658166
const walkSync = (dir, filelist = []) => {
  fs.readdirSync(dir).forEach(file => {
  	
  	//FILTER FOLDERS
  	if(dir==".git" || dir=="node_modules"|| dir=="translations") return;

    filelist = fs.statSync(path.join(dir, file)).isDirectory()
      ? walkSync(path.join(dir, file), filelist)
      : filelist.concat(path.join(dir, file));
  });
  return filelist;
}

//LOAD TRANSLATION KEYS
function loadKeys() {
	var keys = [];
	var translationFile = JSON.parse(fs.readFileSync("./translations/en_GB.json"));
	for (var key in translationFile) {
		keys.push(key);
	}

	return keys;
}

// SEARCH, FILE
function search(fileLocation, searchString) {
	var file = fs.readFileSync(fileLocation);
	if(file.indexOf(searchString) >= 0) {
		return true;
	}
}

function searchEachFile() {
	var findings = [];

	translationKeyList.forEach(function(key){
		var searchResult = [];
		var ctr;

		fileList.forEach(function(file){
			var blackList = [".jpg", ".jpeg", ".png", ".gif", ".svg", ".css", ".scss", ".ttf", ".eot", ".woff", ".woff2", ".md", ".sh", ".DS_Store", ".txt", ".json.schema", ".xml", ".yml", ".gitignore", ".eslintrc", ".eslintignore", "modules.js", "package.json", "translationkey_finder.js"];

			//FILTER FILE TYPES OR FILES
			for (var i=0; i<=blackList.length; i++) {
				if(file.indexOf(blackList[i]) >= 0) return;
			}

			var res = search(file, key);
			if(res) {searchResult.push({[key]: file});}
		});

		if(searchResult.length <= 0) {
			findings.push(key);
		}
		else {
			//console.log(searchResult);
		}
	});

	return findings;
}

//START
translationKeyList = loadKeys();
fileList = walkSync("./");
unusedKeys = searchEachFile();

//console.log("NOT USED KEYS: ", unusedKeys);

//Save list to file...
fs.writeFile("./unused_keys.txt", unusedKeys, function(err) {
    if(err) {
        return console.log(err);
    }

    console.log("The file was saved!");
});